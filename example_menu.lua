-- Table containing all values for settings
ExampleMenu             = {}
ExampleMenu._menu_path  = ModPath .. 'menu.txt'
ExampleMenu._save_path  = SavePath .. 'example_menu.txt'
ExampleMenu._loc_path   = ModPath .. 'loc/'
ExampleMenu._settings   = {
  toggle = true,
  slider = 50,
  multi = 3,
  keybind = nil
}

-- Loads the Localization file(s)

function ExampleMenu.get_loc_file()
  local lang = BLT.Localization:get_language().language

  if lang then
    local file = ExampleMenu._loc_path .. lang .. '.txt'

    if io.file_is_readable(file) then
      return file
    end
  end

  return ExampleMenu._loc_path .. 'en.txt'
end

Hooks:AddHook('LocalizationManagerPostInit', 'example_menu_id',
  function(self)
    local loc_file = ExampleMenu.get_loc_file()
    
    self:load_localization_file(loc_file, false)
  end
)

-- Callback Handlers
-- -- Button
MenuCallbackHandler.btn_clbk    = function(this, item)
  -- todo
end
-- -- Toggle
MenuCallbackHandler.toggle_clbk = function(this, item)
  -- todo
end
-- -- Slider
MenuCallbackHandler.slider_clbk = function(this, item)
  -- todo
  log(item:name())
  log(item:value())
  ExampleMenu._settings[item:name()] = item:value()
end
-- -- Multiple Choice
MenuCallbackHandler.multi_clbk  = function(this, item)
  -- todo
end

function keybind(something)
  -- todo
end

Hooks:Add('MenuManagerInitialize', 'MenuManagerInitialize_mymod', function(menu_manager)
  MenuHelper:LoadFromJsonFile(ExampleMenu._menu_path, ExampleMenu, ExampleMenu._settings)
end)
