# Example Menu for Payday 2 SuperBLT

This mod creates an example of a menu under Mod Options for Payday 2.

This uses the JSON Menu format and examples of all available inputs are provided.

BLT Localization Language Codes

```
en
cht
de
es
fr
id
it
nl
pl
pt-br
ru
sv
tr
chs
```